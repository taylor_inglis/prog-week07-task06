﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task06
{
    class Program
    {
        static void Main(string[] args)
        {
            text();
            text();
            text();
            text();
            text();
        }
        static string method(int number)
        {
            var output = string.Empty;
            if (number % 2 == 0)
                output = "The value is even";
            else
                output = "The value is odd";
            return output;
        }
        static void text()
        {
            Console.WriteLine("Please enter a number");
            var number = Console.ReadLine();
            var output = Program.method(int.Parse(number));
            Console.WriteLine(output);
        }

    }
}
